#include <stdlib.h>
#include <stdio.h>

int is_prime(long n);
int nth_prime(int n);

int main(int argc, char **argv) {
    for (int i = 1; i < argc; i++) {
        int n = strtol(argv[i], NULL, 10);
        printf("%dth prime is %d\n", n, nth_prime(n));
    }
}

int nth_prime(int n) {

    int prime_count = 0;
    int i;

    for (i = 2; prime_count != n; i++) {
        if (is_prime(i)) prime_count++;
    }

    return i - 1;

}

int is_prime(long n) { // assuming input is > 1
    for (long i = 2; i < n; i++) {
        if (n % i == 0) return 0;
    }
    return 1;
}
