#include <stdio.h>

int evenly_divisible(int n);
int lowest_number_evenly_divisible();

int main() {
    printf("%d\n", lowest_number_evenly_divisible());
}

int evenly_divisible(int n) {

    for (int i = 2; i <= 20; i++) {
        if (n % i != 0) return 0;
    }

    return 1;

}

int lowest_number_evenly_divisible() {
    int n = 1;
    while (1) {
        if (evenly_divisible(n)){
            return n;
        }
        n++;
    }
    return 0;
}
