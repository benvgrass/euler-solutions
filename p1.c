int main(int argc, char *argv[]) {
    int total = 0;
    int x = 0;
    while (x < 1000) {
        if ( (x % 3 == 0) || (x % 5 == 0) ) {
            total = total + x;
        }
        x++;
    }
    printf("%d\n", total);
    return 0;
}
