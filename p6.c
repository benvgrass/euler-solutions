#include <stdio.h>
#include <stdlib.h>

long sum_of_squares(int n);
long square_of_sum(int n);
long difference_of_sums(int n);

int main(int argc, char **argv) {
    for (int i = 1; i < argc; i++) {
        printf("%ld\n", difference_of_sums(strtol(argv[i], NULL, 10)));
    }
}

long difference_of_sums(int n) {
    return labs(sum_of_squares(n) - square_of_sum(n));
}

long sum_of_squares(int n) {
    long sum = 0;

    for (int i = 1; i <= n; i++) {
        sum += i * i;
    }

    return sum;
}

long square_of_sum(int n) {
    long sum = 0;

    for (int i = 1; i <= n; i++) {
        sum += i;
    }

    return sum * sum;
}
