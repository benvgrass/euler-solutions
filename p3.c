#include <stdio.h>
#include <stdlib.h>

/*  Project Euler Question 3
    Input: 600851475143
    Output: 71 839 1471 6857
*/

int is_prime(long n) { // assuming input is > 1
    for (long i = 2; i < n; i++) {
        if (n % i == 0) return 0;
    }
    return 1;
}

void prime_factorization(long n, int *result, int *size) {
    int c = 2;
    int result_index = 0;
    while (n != 1) {
        if (n % c == 0 && is_prime(c)) {
            result[result_index++] = c;
            n = n / c;
        } else {
            c++;
        }
    }
    *size = result_index; // number of entries in array
}

int main(int argc, char **argv) {
    long n = strtol(argv[1], NULL, 10);
    int results[20];
    int size;
    prime_factorization(n, results, &size);
    for (int i = 0; i < size; i++) {
        printf("%d ", results[i]);
    }
    printf("\n");
}
