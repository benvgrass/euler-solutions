fib = {0: 1, 1: 1}

def fibonacci(n):
    if n in fib: return fib[n]
    else:
        fib[n] = fibonacci(n-1) + fibonacci(n-2)
        return fib[n]

def get_evens(): # does the actual problem
    n = 0
    total = 0

    while fibonacci(n) < 4000000:
        if fibonacci(n) % 2 == 0:
            total = total + fibonacci(n)
        n = n + 1

    return total

print(get_evens())
