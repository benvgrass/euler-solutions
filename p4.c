#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*
  Find largest palindrome that is a product of 2 3-digit integers
  Input: None
  Output: 906609
*/

int num_digits(int n);
int is_palindrome(int n);
int largest_palindrome();

int main(int argc, char **argv) {
    int palindrome = largest_palindrome();
    printf("Largest palindrome that is a product of 2 3-digit integers is: %d\n",
        palindrome);
}

int largest_palindrome() {
    int largest_palindrome = 0;
    for (int x = 100; x < 1000; x++) {
        for (int y = 100; y < 1000; y++) {
            int n = x * y;
            if (n > largest_palindrome && is_palindrome(n)) largest_palindrome = n;
        }
    }
    return largest_palindrome;
}

int is_palindrome(int n) { // check if n is a palindrome
    int digits = num_digits(n);

    char str_num[digits];
    sprintf(str_num, "%d", n);

    int step = digits/2;

    for (int i = 0; i < step; i++) {
        if (str_num[i] != str_num[(digits - 1) - i]) return 0;
    }

    return 1;

}

int num_digits(int n) { // find number of digits in n
    return floor(log10(abs(n))) + 1;
}
